package be.kdg.shop.sale;

import java.util.ArrayList;
import java.util.List;

public class Sale {
	private List<SalesLineItem> lines = new ArrayList<>();


	public void add(SalesLineItem line) {
		lines.add(line);
	}

	public double getTotal() {
		double total=0.0;
		for (SalesLineItem line:lines){
			total+=line.getProduct().getPrice()* line.getQuantity();
		}
		return total;
	}
}
