package be.kdg.shop;

import be.kdg.shop.sale.*;

public class Shop {

	public static void main(String[] args) {
		Sale sale=new Sale();
		Product orval = new Product("Orval", 2.65);
		Product leonidas = new Product("Leonidas pralines 300g",8.82);
		sale.add(new SalesLineItem(new Product("Orval", 2.45),24));
		sale.add(new SalesLineItem(new Product("Leonidas pralines 300g",8.82),1));
		System.out.println(String.format("Amounnt due: €%.2f", sale.getTotal()));
	}
}
