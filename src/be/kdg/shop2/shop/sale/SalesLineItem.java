package be.kdg.shop2.shop.sale;

public class SalesLineItem {
	private int quantity;
	private Product product;

	public SalesLineItem(Product product, int quantity) {
		this.quantity = quantity;
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public Product getProduct() {
		return product;
	}

	public double getTotal() {
		return quantity+product.getPrice();
	}
}
